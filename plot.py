import numpy as np
import matplotlib.pyplot as plt
import numpy as np
N=2

#file = open("./Spectra/Spectrum_Ns20_Nn3_L2_N2_maxOcc2_omega10.000000_sigma_0.100000_gdd0.000000_maxRange10_PBC1_gMax300.000000",'r')
file = open("./Spectra/Spectrum_Ns24_Nn3_L2_N2_maxOcc2_sigma_0.100000_gdd0.000000_maxRange10_PBC1_gMax300.000000",'r')
#file = open("./Spectra/Spectrum_Ns24_Nn3_L2_N2_maxOcc2_sigma_0.100000_gdd0.000000_maxRange10_PBC0_gMax300.000000",'r')
#file = open("./Spectra/Spectrum_Ns24_Nn3_L2_N2_maxOcc2_omega10.000000_sigma_0.100000_gdd4.000000_maxRange10_PBC1_gMax300.000000",'r')
gs,Es = [],[]

lines = file.read().splitlines()
line = lines[1]

grow = float(line.split()[0])
Erow = [float(x) for x in line.split()[1:]]
for E in Erow:
    gs.append(grow)
    Es.append(E)
    
for i in range(2,len(lines)):
    line=lines[i]
    grow = float(line.split()[0])
    Erow = [float(x) for x in line.split()[1:]]
    for E in Erow:
        gs.append(grow)
        Es.append(E)
        
plt.plot(gs,[E/N/np.pi**2*6 for E in Es],'o',markersize=1)
plt.ylim([-1,17.2])
plt.grid()
plt.show()
