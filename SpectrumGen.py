Ns,Nn = 10,3
L,N,maxOcc = 1,3,3
sigma=0.1
gdd=0.0
maxRange=10
PBC=0
omega=60
gMax = 300

g0,gl,dg=-600,-400,1.0

No = Nn*Ns
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

from numpy import zeros,loadtxt,copy,sqrt,ones,linspace,pi
from scipy.sparse import lil_array, load_npz, csr_matrix
import scipy

def delete_rows_csr(mat, indices):
    if not isinstance(mat, csr_matrix):
        raise ValueError("works only for CSR format -- use .tocsr() first")
    indices = list(indices)
    mask = ones(mat.shape[0], dtype=bool)
    mask[indices] = False
    return mat[mask]       

def ind(j,n):
    return j*Nn+n+int(Nn*Ns/2);
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
base = []
NssVirtual = 0

if N==2:
    state = zeros(No,dtype=int)
    base.append(tuple(state))
    NssVirtual+=1
    
    for i in range(No):
        state = zeros(No,dtype=int)
        state[i]+=1
        base.append(tuple(state))
        NssVirtual+=1
        
    for i in range(No):
        for ip in range(i,No):
            state = zeros(No,dtype=int)
            state[i]+=1
            state[ip]+=1
            base.append(tuple(state))

if N==3:
    state = zeros(No,dtype=int)
    base.append(tuple(state))
    NssVirtual+=1

    for i in range(No):
        state = zeros(No,dtype=int)
        state[i]+=1
        base.append(tuple(state))
        NssVirtual+=1
        
    for i in range(No):
        for ip in range(i,No):
            state = zeros(No,dtype=int)
            state[i]+=1
            state[ip]+=1
            base.append(tuple(state))  
            NssVirtual+=1

    for i in range(No):
        for ip in range(i,No):
            for ipp in range(ip,No):
                state = zeros(No,dtype=int)
                state[i]+=1
                state[ip]+=1
                state[ipp]+=1
                base.append(tuple(state))     
        
Nss = len(base)
######################################################
b = {}

for j in range(-int(Ns/2),int(Ns/2)):
    for n in range(Nn):
        index = ind(j,n)
        #print(j,n,index)
        #b[index] = np.zeros([Nss,Nss],dtype=float)
        b[index] = lil_array((Nss, Nss),dtype=float)
        for i in range(Nss):
                basestate = copy(base[i])
                if basestate[index]>0:
                    coef = sqrt(basestate[index])
                    basestate[index]-=1
                    #print(basestate)
                    ip = base.index(tuple(basestate))
                    b[index][ip,i]=coef
        b[index].tocsr()       
        ############################################
        # HK = load_npz("./HamiltonianMatrices/K_L%.3f_N%i_Ns%i_Nn%i_PBC%i.npz"%(L,N,Ns,Nn,PBC))
# HV = load_npz("./HamiltonianMatrices/V_L%.3f_N%i_Ns%i_Nn%i_PBC%i.npz"%(L,N,Ns,Nn,PBC))
# HUps = load_npz("./HamiltonianMatrices/Ups_L%.3f_N%i_Ns%i_Nn%i_PBC%i.npz"%(L,N,Ns,Nn,PBC))
# HU = load_npz("./HamiltonianMatrices/U_L%.3f_N%i_Ns%i_Nn%i_PBC%i.npz"%(L,N,Ns,Nn,PBC))

HK = load_npz("./HamiltonianMatrices/K_L%.3f_N%i_Ns%i_Nn%i_PBC%i.npz"%(L,N,Ns,Nn,PBC))
HV = load_npz("./HamiltonianMatrices/V_L%.3f_N%i_Ns%i_Nn%i_PBC%i.npz"%(L,N,Ns,Nn,PBC))
HUps = load_npz("./HamiltonianMatrices/Ups_L%.3f_N%i_Ns%i_Nn%i_PBC%i.npz"%(L,N,Ns,Nn,PBC))
HU = load_npz("./HamiltonianMatrices/U_L%.3f_N%i_Ns%i_Nn%i_PBC%i.npz"%(L,N,Ns,Nn,PBC))
HUdd = load_npz("./HamiltonianMatrices/Udd_L%.3f_N%i_Ns%i_Nn%i_PBC%i.npz"%(L,N,Ns,Nn,PBC))
#######################################################

import numpy.linalg as LA
import numpy as np
# file = open('Spectra/Spectrum_Ns%i_Nn%i_L%i_N%i_maxOcc%i_omega%.6f_sigma_%.6f_gdd%.6f_maxRange%i_PBC%i_gMax%.6f'%(Ns,Nn,L,N,maxOcc,omega,sigma,gdd,maxRange,PBC,gMax),'w')
# file.write('gamma\tEs\n')
# file.close()

for g in np.arange(g0,gl+0.01,dg):
    print(g,end=',')
    H = HK - omega**2/2*HV + 20000*HUps + g*HU + gdd*HUdd
    eigenvalues, eigenvectors = LA.eigh(H.todense())
    file = open('Spectra/Spectrum_Ns%i_Nn%i_L%i_N%i_maxOcc%i_omega%.6f_sigma_%.6f_gdd%.6f_maxRange%i_PBC%i_gMax%.6f'%(Ns,Nn,L,N,maxOcc,omega,sigma,gdd,maxRange,PBC,gMax),'a')
    file.write('%.6f\t'%g)
    for E in eigenvalues:
        if (-1*N*pi**2/6 < E) and (E <4*17*N*pi**2/6):
            file.write('%.12f\t'%E)
    file.write('\n')
    file.close()
    

